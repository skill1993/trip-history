//
//  ViewController.swift
//  trip history
//
//  Created by Никита Журавлев on 09/11/2018.
//  Copyright © 2018 Никита Журавлев. All rights reserved.
//

import UIKit
import MapKit

class PlaceViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    // блок переменных с которыми работаем
    var placeName: String = "Пустая ячейка"
    var moneySpend: Int = 0
    var currency: String = "$"
    var newModCurr: Currency = .dollar
    var placeLat: CLLocationDegrees = 0.000
    var placeLong: CLLocationDegrees = 0.000
    var rating: Int = 0
    var transpotationType: String = "✈️"
    var transType2: TransportType = .plane
    var placeDescription: String = "С этими опционалами только голову ломать"
    // конец блока с которыми работаем
    var pay: CurrencyResult? = nil
    var currentExpense: Money! {
        didSet { expenseLBL.text = currentExpense.description }
    }
    
    // Привязали аутлеты
    @IBOutlet weak var placeNameTF: UITextField!
    @IBOutlet weak var ratingLBL: UILabel!
    @IBOutlet weak var ratingStepper: UIStepper!
    @IBOutlet weak var locationLBL: UILabel!
    @IBOutlet weak var expenseLBL: UILabel!
    @IBOutlet weak var transportSegmentView: UISegmentedControl!
    @IBOutlet weak var descriptionTF: UITextView!
    
    // Добавили действие изменения значения
    @IBAction func stepperChange(_ sender: Any) {
        if ratingStepper.value <= 5 {
            ratingLBL.text = String(Int(ratingStepper.value))
            rating = Int(ratingStepper.value)
        } else if ratingStepper.value > 5 {
            ratingStepper.value = 5
            ratingLBL.text = String(Int(ratingStepper.value))
            rating = Int(ratingStepper.value)
        }
    }
    
    // Unwind SEGUE с экрана затрат
    @IBAction func backFromExpenceVC(sender: UIStoryboardSegue) {
        // sender.sourceViewController has changed to sender.source
        if let placeVC = sender.source as? PlaceCostViewController {
            moneySpend = placeVC.tempMoney
            currency = placeVC.tempCurrency
            newModCurr = placeVC.tmpCur2
            expenseLBL.text = String(Int(moneySpend)) + " " + newModCurr.sign()
            currentExpense = placeVC.currencyExpense
        }
    }
    
    // Unwind SEGUE с карты
    @IBAction func backFromMapVC(sender: UIStoryboardSegue) {
        if let placeVC = sender.source as? MapViewController {
            placeLat = placeVC.myLatitude
            placeLong = placeVC.myLongtitude
            placeVC.choosePoint = false
        }
        locationLBL.text = "\(Double(round(100*placeLat)/100)),\(Double(round(100*placeLong)/100))"
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "show map":
            let mapVC = segue.destination as? MapViewController
            
            mapVC?.choosePoint = true
        case "show expence":
            let curVC = segue.destination as? PlaceCostViewController
            print(pay!)
            curVC?.pay = pay
        default: break
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ratingLBL.text = String(Int(ratingStepper.value))
        expenseLBL.text = String(Int(moneySpend)) + " " + newModCurr.sign()
        transpotationType = transportSegmentView.titleForSegment(at: 0) ?? transpotationType
        locationLBL.text = ("0.00,0.00")
        descriptionTF.delegate = self
        descriptionTF.text = "Введите описание..."
        descriptionTF.textColor = .lightGray
        self.transportSegmentView.addTarget(self, action: #selector(selectedSegment), for: .valueChanged)
    }
    
    @objc func selectedSegment(target: UISegmentedControl) {
        if target == self.transportSegmentView {
            transpotationType = transportSegmentView.titleForSegment(at: transportSegmentView.selectedSegmentIndex) ?? transpotationType
            switch transpotationType {
            case "✈️": transType2 = .plane
            case "🚂": transType2 = .train
            case "🚙": transType2 = .car
            default: transType2 = .plane
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        placeName = placeNameTF.text!
        print(placeName)
        self.view.endEditing(true)

        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            placeDescription = descriptionTF.text!
            return false
        } else {
            return true
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (descriptionTF.text == "Введите описание..."){
            descriptionTF.text = ""
            descriptionTF.textColor = .black
        }
        descriptionTF.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if (descriptionTF.text == ""){
            descriptionTF.text = "Введите описание..."
            descriptionTF.textColor = .lightGray
        }
        descriptionTF.resignFirstResponder()
    }
    //: # Функции которые мы использовали для тренировки работы с Interface Builder
//    @IBOutlet var colorControl: UISegmentedControl!
//
//    func selectedColor() -> UIColor {
//        switch colorControl.selectedSegmentIndex {
//        case 0:
//            return .red
//        case 1:
//            return .blue
//        default:
//            return .lightGray
//        }
//    }
//
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        guard let identifier = segue.identifier else { return }
//        switch identifier {
//        case "ColorScreen":
//            if let viewController = segue.destination as? ColorViewController {
//                viewController.backgroundColor = selectedColor()
//            }
//        default:
//            return
//        }
//    }
    
}

