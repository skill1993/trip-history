//
//  MapViewController.swift
//  trip history
//
//  Created by Никита Журавлев on 14/11/2018.
//  Copyright © 2018 Никита Журавлев. All rights reserved.
//

import UIKit
//: Для работы с картами необходимо во вью контроллер импортировать модуль MapKit
import MapKit

class MapViewController: UIViewController, UIGestureRecognizerDelegate {
    
    var choosePoint: Bool = false
    
    var pin = MKPointAnnotation()
    
    var annotations: [MKPointAnnotation]!

    var myLatitude: CLLocationDegrees = 0.0
    var myLongtitude: CLLocationDegrees = 0.0
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var doneButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        doneButton.layer.cornerRadius = 8
/*
        let moscowPlace = Place(name: "Moscow", lat: 55.668, long: 37.609, rating: 5, moneyPaid: Place.moneyPaid(moneyLost: 200, currencyType: .EUR), transportType: .TRN, tripDescription: "OTLICHNO")
        let moscow = MKPointAnnotation()
        moscow.coordinate = moscowPlace.coordinate
        /*
        let ekatPlace = Place(name: "Екатеринбург", lat: 56.829, long: 60.596, rating: 5, moneyLost: 250, transportType: .AIR, currencyType: .USD, tripDescription: "HOROSHO")
        let ekat = MKPointAnnotation()
        ekat.coordinate = ekatPlace.coordinate
        
        let vladiPlace = Place(name: "Владивосток", lat: 43.120, long: 131.957, rating: 4, moneyLost: 1000, transportType: .CAR, currencyType: .EUR, tripDescription: "OOOCHEN DOROGO")
        let vladi = MKPointAnnotation()
        vladi.coordinate = vladiPlace.coordinate
        
        let myPlace = Place(name: "MOYA OSTANOVOCHKA", lat: 54.665, long: 36.607, rating: 5, moneyLost: 1, transportType: .CAR, currencyType: .RUR, tripDescription: "НУ ПРОСТО ШИДЕВАР")
        let myP = MKPointAnnotation()
        myP.coordinate = myPlace.coordinate
        */*/

        if choosePoint == true {
            setMapView()
        } else if choosePoint == false {
            mapView.addAnnotations(annotations)
        }
        // Do any additional setup after loading the view.
    }
    /*
    func setMapView() {
        print("startedSettingMap")
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(MapViewController.handleLongPress(gestureRecognizer:)))
        lpgr.minimumPressDuration = 0.01
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        self.mapView.addGestureRecognizer(lpgr)
        print("endedSettingMap")
    }*/
    
    func setMapView() {
        print("startedSettingMap")
        let lpgr = UITapGestureRecognizer(target: self, action: #selector(MapViewController.handleTap(gestureRecognizer:)))
        lpgr.delegate = self
        self.mapView.addGestureRecognizer(lpgr)
        print("endedSettingMap")
    }
    
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if gestureRecognizer.state != UIGestureRecognizer.State.ended{
            self.mapView.removeAnnotation(pin)
            let touchLocation = gestureRecognizer.location(in: mapView)
            let locationCoordinate = mapView.convert(touchLocation, toCoordinateFrom: mapView)
            print("Tapped at lat: \(locationCoordinate.latitude) and long: \(locationCoordinate.longitude)")
            myLatitude = locationCoordinate.latitude
            myLongtitude = locationCoordinate.longitude
            self.pin.coordinate = CLLocationCoordinate2D(latitude: myLatitude,longitude: myLongtitude)
            self.mapView.addAnnotation(pin)
            return
        }
        
        if gestureRecognizer.state != UIGestureRecognizer.State.began {
            return
        }
    }
    
    @objc func handleTap(gestureRecognizer: UITapGestureRecognizer){
        if gestureRecognizer.state == .ended {
            self.mapView.removeAnnotation(pin)
            let touchLocation = gestureRecognizer.location(in: mapView)
            let locationCoordinate = mapView.convert(touchLocation, toCoordinateFrom: mapView)
            print("Tapped at lat: \(locationCoordinate.latitude) and long: \(locationCoordinate.longitude)")
            myLatitude = locationCoordinate.latitude
            myLongtitude = locationCoordinate.longitude
            self.pin.coordinate = CLLocationCoordinate2D(latitude: myLatitude,longitude: myLongtitude)
            self.mapView.addAnnotation(pin)
            return
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
