//
//  PlacesViewController.swift
//  trip history
//
//  Created by Никита Журавлев on 28/11/2018.
//  Copyright © 2018 Никита Журавлев. All rights reserved.
//

import UIKit
import MapKit

class PlacesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate {
    
    var cities: [String]?
    // Наш массив объектов))))
    var places: [Place] = []
    var annos: [MKPointAnnotation] = []
    var pay: CurrencyResult? = nil
    var row: Int = 0
    
    var tripTitle = ""
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = tripTitle
        tableView.estimatedRowHeight = 125
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInset.top = 16
        // Метод работы с данными в коде
        tableView.dataSource = self
        // Метод делегирования кодом
        tableView.delegate = self
        navigationController?.delegate = self
        self.tableView.reloadData()
        // Do any additional setup after loading the view.
    }
    
    // protocol method в этом методе считаем сколько ячеек

    
    @IBAction func backFromStopVCbySave(sender: UIStoryboardSegue){
        if let placeUPDVC = sender.source as? PlaceViewController {
            if placeUPDVC.currentExpense != nil {
                let tempPlace = Place(name: placeUPDVC.placeName,
                                      lat: placeUPDVC.placeLat,
                                      long: placeUPDVC.placeLong,
                                      rating: placeUPDVC.rating,
                                      moneyLost: placeUPDVC.currentExpense,
                                      transportType: placeUPDVC.transType2,
                                      tripDescription: placeUPDVC.placeDescription)
                let tempAnnot = MKPointAnnotation()
                tempAnnot.coordinate = tempPlace.coordinate
                self.places.append(tempPlace)
                self.annos.append(tempAnnot)
                print(places)
                tableView.insertRows(at: [IndexPath(row: places.count - 1, section: 0)], with: .fade)
                
            }
            /*if placeUPDVC.placeName != nil {
                if (self.cities?.append(placeUPDVC.placeName!)) == nil {
                    self.cities = [placeUPDVC.placeName!]
                }
                print(self.cities!)
                self.tableView.reloadData()
            }*/
        }
    }
    
    @IBAction func backFromStopVCbyCancel(sender: UIStoryboardSegue){
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*if cities?.count == nil {
            return 0
        } else {
            return cities!.count
        }*/
        
        return places.count

    }
    
    // protocol method а тута возвращаем саму ячейку
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        /*if cities?[indexPath.row] != nil {
            cell.textLabel?.text = cities![indexPath.row]
        }*/
        guard let placeCell = cell as? PlacesCell else {
            return cell
        }

        let blazes = places[indexPath.row]
        placeCell.confugure(for: blazes)
        
        return placeCell

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let blazes = self.places[indexPath.row]
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if self.isMovingToParent {
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "show stop":
            let pVC = segue.destination as? PlaceViewController
                        print(pay!)
            pVC?.pay = pay
        case "add new stop":
            let pVC = segue.destination as? PlaceViewController
            print(pay!)
            pVC?.pay = pay
        default:
            break
        }

    }
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        (viewController as? TripsViewController)?.newTrips[row].places = places
        (viewController as? TripsViewController)?.newTrips[row].rating = ((viewController as? TripsViewController)?.findRatingForTrip(places: ((viewController as? TripsViewController)?.newTrips[row].places)!))!
        (viewController as? TripsViewController)?.aannos = annos
        (viewController as? TripsViewController)?.tableView.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

