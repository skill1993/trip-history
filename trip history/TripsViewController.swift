//
//  TripsViewController.swift
//  trip history
//
//  Created by Никита Журавлев on 24/12/2018.
//  Copyright © 2018 Никита Журавлев. All rights reserved.
//

import UIKit
import MapKit

class TripsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    var trips: [String]?
    var text: String?
    var index: Int = 0
    var tripTitle = ""
    
    let currencyService = CurrencyService()
    var pay: CurrencyResult? = nil
    
    var newTrips: [Trip] = []{
        didSet {
            let hideTable = newTrips.count == 0
            tableView.isHidden = hideTable
            noContentLable.isHidden = !hideTable
        }
    }
    
    var aannos: [MKPointAnnotation] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addTripButton: UIBarButtonItem!
    @IBOutlet weak var noContentLable: UILabel!
    
    var tripsForced = ["Италия"," Россия", "Китай", "Япония"]

    override func viewDidLoad() {
        super.viewDidLoad()
        obtain()
        
        tableView.estimatedRowHeight = 87
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInset.top = 16
        // Метод работы с данными в коде
        tableView.dataSource = self
        
        // Метод делегирования кодом
        tableView.delegate = self

        // Do any additional setup after loading the view.
        self.tableView.reloadData()
    }
    
    @IBAction func addNewTrip() {
        
        let alertViewController = UIAlertController(title: "Добавить новое путешествие",
                                                    message: "Укажите название и комментарий:",
                                                    preferredStyle: .alert)
        
        alertViewController.addTextField { textfield in
            textfield.placeholder = "Название"
        }
        
        alertViewController.addTextField { textfield in
            textfield.placeholder = "Комментарий"
        }
        
        alertViewController.addAction(UIAlertAction(title: "Отмена",
                                                    style: .cancel,
                                                    handler: nil))

        alertViewController.addAction(UIAlertAction(title: "Добавить",
                                                    style: .default,
                                                    handler: { _ in
                                                        guard let nameTextField = alertViewController.textFields?[0],
                                                            let commentTextField = alertViewController.textFields?[1] else {
                                                            return
                                                        }
                                                        
                                                        guard let name = nameTextField.text,
                                                            let comment = commentTextField.text else {
                                                                return
                                                        }
                                                        
                                                        self.addTripToArray(with: name, comment: comment)
        }))
        
        present(alertViewController, animated: true, completion: nil)
        
        // Блок логики добавления путешествия до просмотра видосов по этой теме))))
        
        /*
        let alert = UIAlertController(title: "Добавление", message: "нового путешествия", preferredStyle: UIAlertController.Style.alert)
        alert.addTextField(configurationHandler: {(textField: UITextField!) in
            textField.placeholder = "Введите название страны: "
            textField.isSecureTextEntry = false
        })
        alert.addAction(UIAlertAction(title: "Добавить", style: UIAlertAction.Style.default, handler: { action in
            if let textFieldText = alert.textFields?.first?.text {
                if textFieldText != "" {
                    self.text = textFieldText
                    if (self.trips?.append(self.text!)) == nil {
                        self.trips = [self.text!]
                        print(self.trips!)
                    }
                    print("TextField text \(textFieldText) \\ \(self.text!) \\ \(self.trips!)")
                    self.tableView.reloadData()
                }
            }

        }))
        alert.addAction(UIAlertAction(title: "Отменить", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)*/
    }
    
    func addTripToArray(with name: String, comment: String) {
        let trip = Trip(comment: comment, name: name, places: [])
        newTrips.append(trip)
        tripTitle = trip.name
        
        tableView.insertRows(at: [IndexPath(row: newTrips.count - 1, section: 0)],
                             with: .fade)
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*if trips?.count == nil {
            return 0
        } else {
            return trips!.count
        }
        */
        return newTrips.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripCell", for: indexPath)
        /*if trips?[indexPath.row] != "" {
            cell.textLabel?.text = trips?[indexPath.row]
        }*/
        
        guard let tripCell = cell as? TripCell else {
            return cell
        }
        
        let trip = newTrips[indexPath.row]
        
        tripCell.configure(for: trip)
        
        return tripCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("Row \(indexPath.row) selected")
        tableView.deselectRow(at: indexPath, animated: false)
        // Здесь пытаемся поменять название путешествия

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
        case "show stops on map":
            let mapVC = segue.destination as? MapViewController
            mapVC?.choosePoint = false
            mapVC?.annotations = aannos
        case "show places":
            let tripRow = self.tableView.indexPath(for: sender as! UITableViewCell)
            let placesVC = segue.destination as? PlacesViewController
            placesVC?.self.row = (tripRow?.row)!
            placesVC?.self.tripTitle = newTrips[(tripRow?.row)!].name
            placesVC?.self.places = newTrips[(tripRow?.row)!].places
            placesVC?.self.annos = aannos
            print(pay!)
            placesVC?.self.pay = pay
        default: break
        }
        /*
        if segue.identifier == "show stops on map"/* && palaces != nil */{
            let mapVC = segue.destination as? MapViewController
            
            mapVC?.choosePoint = false
            
            let moscowPlace = Place(name: "Moscow", lat: 55.668, long: 37.609, rating: 5, moneyLost: 200, currencyType: "₽", transportType: "✈️", tripDescription: "OTLICHNO")
            let moscow = MKPointAnnotation()
            moscow.coordinate = moscowPlace.coordinate!
            
            let ekatPlace = Place(name: "Екатеринбург", lat: 56.829, long: 60.596, rating: 4, moneyLost: 250, currencyType: "$",transportType: "✈️", tripDescription: "HOROSHO")
            let ekat = MKPointAnnotation()
            ekat.coordinate = ekatPlace.coordinate!
            
            let vladiPlace = Place(name: "Владивосток", lat: 43.120, long: 131.957, rating: 3, moneyLost: 1000, currencyType: "€", transportType: "🚙", tripDescription: "OOOCHEN DOROGO")
            let vladi = MKPointAnnotation()
            vladi.coordinate = vladiPlace.coordinate!
            
            let myPlace = Place(name: "MOYA OSTANOVOCHKA", lat: 54.665, long: 36.607, rating: 5, moneyLost: 1, currencyType: "₽", transportType: "🚙", tripDescription: "НУ ПРОСТО ШИДЕВАР")
            let myP = MKPointAnnotation()
            myP.coordinate = myPlace.coordinate!
            
            mapVC?.annotations = [moscow,ekat,vladi,myP]
        }
        
        if segue.identifier == "show places" {
            let placesVC = segue.destination as? PlacesViewController
            
            if palaces != nil {
                placesVC!.places = palaces
            }
        }
        */
    }
    
    @IBAction func backFromMapVC(sender: UIStoryboardSegue){
        if let placeVC = sender.source as? MapViewController {
            placeVC.mapView.removeAnnotations(placeVC.annotations)
        }
    }
    
    @IBAction func backFromPlacesVC(sender: UIStoryboardSegue){

    }
    
    func findRatingForTrip(places: [Place]) -> Int {
        var placeRating: Int = 0
        if places.count > 0 {
            for i in 0..<places.count {
                placeRating += places[i].rating
            }
            placeRating = placeRating / places.count
            return placeRating
        } else {
            return 0
        }
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func obtain() {
        print("start obtain")
        currencyService.obtainCurrency2(for: [.dollar, .rouble, .euro]) { result in
            switch result {
            case .success(let payload):
                print(payload)
                self.pay = payload
            case .failure(let error):
                print(error)
            }
        }
        print("ended obtain")
    }
    
}
