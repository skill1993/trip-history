//
//  CurrencyViewController.swift
//  trip history
//
//  Created by Никита Журавлев on 05/01/2019.
//  Copyright © 2019 Никита Журавлев. All rights reserved.
//

import UIKit

class CurrencyViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var currencyLBL: UILabel!
    @IBOutlet weak var currencyTable: UITableView!
    @IBOutlet weak var backButton: UIButton!

    let cursesArray: [Currency] = [.euro, .dollar]
    var pay: CurrencyResult? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        /*
        currencyService.obtainUSDCurrency(for: [.dollar, .rouble, .euro]) { result in
            switch result {
            case .success(let payload):
                print(payload)
            case .failure(let error):
                print(error)
            }
        }
        */

        backButton.layer.cornerRadius = 8
        currencyTable.estimatedRowHeight = 75
        currencyTable.rowHeight = UITableView.automaticDimension
        currencyTable.contentInset.top = 16
        // Метод работы с данными в коде
        currencyTable.dataSource = self
        // Метод делегирования кодом
        currencyTable.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cursesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = currencyTable.dequeueReusableCell(withIdentifier: "CurrencyCell", for: indexPath)
        

        
        guard let curCell = cell as? CurrencyCell else {
            return cell
        }
        
        let curses = cursesArray[indexPath.row]

        curCell.configure(for: curses, pay: pay!)

        return curCell
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
