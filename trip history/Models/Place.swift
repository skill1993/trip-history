//
//  Place.swift
//  trip history
//
//  Created by Никита Журавлев on 14/11/2018.
//  Copyright © 2018 Никита Журавлев. All rights reserved.
//

import Foundation
// для работы с картами необходим модуль CoreLocation
import CoreLocation

enum TransportType {
    case plane
    case train
    case car
}

// создаем класс Place (Остановка) на основе которого будем потом получать его экземпляры
class Place {
    // название точки остановки
    let name: String
    // координата нашей точки, принимает широту и долготу
    let coordinate: CLLocationCoordinate2D
    // рейтинг остановки
    let rating: Int
    // потраченная сумма с валютой
    let moneyLost: Money
    // тип транспорта
    let transportType: TransportType
    // описание нашего бэдтрипа
    let tripDescription: String
    
    init(name: String, lat: CLLocationDegrees, long: CLLocationDegrees, rating: Int, moneyLost: Money, transportType: TransportType, tripDescription: String) {
        self.name = name
        self.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
        self.rating = rating
        self.moneyLost = moneyLost
        self.transportType = transportType
        self.tripDescription = tripDescription
    }
    
}

extension Place: CustomStringConvertible {
    var description: String {
        return """
        Name: \(name)
        Coordinate: \(coordinate)
        Rating: \(rating)
        Money Spend: \(moneyLost)
        Transport: \(transportType)
        Description: \(tripDescription)
        """
    }
}
