//
//  Trips.swift
//  trip history
//
//  Created by Никита Журавлев on 01/01/2019.
//  Copyright © 2019 Никита Журавлев. All rights reserved.
//

import Foundation

class Trip {
    var comment: String
    var name: String
    var places: [Place]
    var rating: Int = 0
    
    init (comment: String, name: String, places: [Place]) {
        self.comment = comment
        self.name = name
        self.places = places
    }
}
