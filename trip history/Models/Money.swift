//
//  Money.swift
//  trip history
//
//  Created by Никита Журавлев on 04/01/2019.
//  Copyright © 2019 Никита Журавлев. All rights reserved.
//

import Foundation

enum Currency: String {
    case euro = "EUR"
    case dollar = "USD"
    case rouble = "RUB"
    
    func sign() -> String {
        switch self {
        case .euro: return "€"
        case .dollar: return "$"
        case .rouble: return "₽"
        }
    }
}

struct Money {
    let currency: Currency
    let amount: Int
}

extension Money: CustomStringConvertible {
    var description: String {
        let currencyCode = currency.rawValue
        let locale = NSLocale(localeIdentifier: currencyCode)
        let currencySymbol = locale.displayName(forKey: .currencySymbol, value: currencyCode) ?? currencyCode
        return "\(amount) \(currencySymbol)"
    }
}
