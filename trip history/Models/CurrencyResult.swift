//
//  CurrencyResult.swift
//  trip history
//
//  Created by Никита Журавлев on 05/01/2019.
//  Copyright © 2019 Никита Журавлев. All rights reserved.
//

import Foundation

struct CurrencyResult{
    let success: Bool
    let timestamp: Int
    let date: String
    let base: Currency
    let rates: [Currency: Double]
}

extension CurrencyResult: Decodable {
    enum CodingKeys: String, CodingKey {
        case success
        case timestamp
        case date
        case base
        case rates
    }
    
    init(from: Decoder) throws {
        let values = try from.container(keyedBy: CodingKeys.self)
        let baseString = try values.decode(String.self, forKey: .base)
        let ratesStringDict = try values.decode([String: Double].self, forKey: .rates)
        self.success = try values.decode(Bool.self, forKey: .success)
        self.timestamp = try values.decode(Int.self, forKey: .timestamp)
        self.date = try values.decode(String.self, forKey: .date)
        
        guard let base = Currency.init(rawValue: baseString) else {
            throw DecodingError.dataCorruptedError(
            forKey: .base,
            in: values,
            debugDescription: "Invalid base Value")
        }
        self.base = base
        
        var rates: [Currency: Double] = [:]
        
        for (key, value) in ratesStringDict {
            if let r = Currency(rawValue: key) {
                rates[r] = value
            }
        }
        self.rates = rates
    }
}

extension CurrencyResult: CustomStringConvertible {
    var description: String {
        return """
        CurrencyResult
        success: \(success)
        timestamp: \(timestamp)
        date: \(date)
        base: \(base)
        rates: \(rates)
        """
    }
}

struct MyCurrencyResult {
    let disclaimer: String
    let license: String
    let timestamp: Int
    let base: Currency
    let rates: [Currency: Double]
}

extension MyCurrencyResult: Decodable {
    enum CodingKeys: String, CodingKey {
        case disclaimer
        case license
        case timestamp
        case base
        case rates
    }
    
    init(from: Decoder) throws {
        let values = try from.container(keyedBy: CodingKeys.self)
        let baseString = try values.decode(String.self, forKey: .base)
        let ratesStringDict = try values.decode([String: Double].self, forKey: .rates)
        
        self.disclaimer = try values.decode(String.self, forKey: .disclaimer)
        self.license = try values.decode(String.self, forKey: .license)
        self.timestamp = try values.decode(Int.self, forKey: .timestamp)
        
        guard let base = Currency.init(rawValue: baseString) else {
            throw DecodingError.dataCorruptedError(
                forKey: .base,
                in: values,
                debugDescription: "Invalid base Value")
        }
        self.base = base
        
        var rates: [Currency: Double] = [:]
        
        for (key, value) in ratesStringDict {
            if let r = Currency(rawValue: key) {
                rates[r] = value
            }
        }
        self.rates = rates
    }
}

extension MyCurrencyResult: CustomStringConvertible {
    var description: String {
        return """
        MyCurrencyResult
        disclaimer: \(disclaimer)
        license: \(license)
        timestamp: \(timestamp)
        base: \(base)
        rates" \(rates)
        """
    }
}
