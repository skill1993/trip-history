//
//  PlacesCell.swift
//  trip history
//
//  Created by Никита Журавлев on 03/01/2019.
//  Copyright © 2019 Никита Журавлев. All rights reserved.
//

import UIKit

class PlacesCell: UITableViewCell {
    
    @IBOutlet weak var textInfoContainer: UIView!
    @IBOutlet weak var transportImageView: UIView!
    
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var ratingStackView: UIStackView!
    @IBOutlet weak var transportImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textInfoContainer.layer.cornerRadius = 10
        transportImageView.layer.cornerRadius = 11
        clearRating()
    }
    
    func confugure(for place: Place) {
        placeName.text = place.name
        descriptionLabel.text = place.tripDescription
        costLabel.text = ("\(place.moneyLost.currency.sign())\(place.moneyLost.amount)")
            
        for i in 0..<place.rating {
            ratingStackView.arrangedSubviews[i].tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
        }
            
        switch place.transportType {
        case .plane:
            transportImage.image = #imageLiteral(resourceName: "plane")
        case .train:
            transportImage.image = #imageLiteral(resourceName: "train")
        case .car:
            transportImage.image = #imageLiteral(resourceName: "car")
        }
        
    }
    
    override func prepareForReuse() {
        clearRating()
    }
    
    func clearRating() {
        ratingStackView.arrangedSubviews.forEach { star in
            star.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
        }
    }
}
