//
//  Currency Cell.swift
//  trip history
//
//  Created by Никита Журавлев on 05/01/2019.
//  Copyright © 2019 Никита Журавлев. All rights reserved.
//

import UIKit

class CurrencyCell: UITableViewCell {
    
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var curInLbl: UILabel!
    @IBOutlet weak var valueInLbl: UILabel!
    @IBOutlet weak var curOutLbl: UILabel!
    @IBOutlet weak var valueOutLbl: UILabel!
    
    var tempCalc: Double = 0.0
    
    let currencyService = CurrencyService()
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        infoView.layer.cornerRadius = 10
    }
    
    // Тут сконфигурируем ячейку.
    func configure(for currencies: Currency, pay: CurrencyResult) {
        print("started config")
        print("continued")
        /*
        guard case self.pay? != nil else {
            print("pay is nil")
        }*/
        print("myTemp 2")/*
        print(tempCalc)
        curInLbl.text = Currency.rouble.rawValue
        valueInLbl.text = "\(tempCalc)"
        
        curOutLbl.text = currencies.rawValue
        valueOutLbl.text = "1"
        
        */
        tempCalc = pay.rates[Currency.rouble]! * pay.rates[Currency.euro]! / pay.rates[currencies]!
        tempCalc = Double(round(1000*tempCalc)/1000)
        curOutLbl.text = Currency.rouble.rawValue
        valueOutLbl.text = "\(tempCalc)"

        curInLbl.text = currencies.rawValue
        valueInLbl.text = "1.000"
 
        print("ended")
    }
    
}
