//
//  TripCell.swift
//  trip history
//
//  Created by Никита Журавлев on 01/01/2019.
//  Copyright © 2019 Никита Журавлев. All rights reserved.
//

import UIKit

class TripCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var ratingStackView: UIStackView!
    @IBOutlet weak var infoContainerView: UIView!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        infoContainerView.layer.cornerRadius = 10
        clearRating()
    }
    
    func configure(for trip: Trip) {
        nameLabel.text = trip.name
        descriptionLabel.text = trip.comment
        
        for i in 0..<trip.rating {
            ratingStackView.arrangedSubviews[i].tintColor = #colorLiteral(red: 1, green: 0.5843137255, blue: 0, alpha: 1)
        }
    }
    
    override func prepareForReuse() {
        clearRating()
    }
    
    
    func clearRating() {
        ratingStackView.arrangedSubviews.forEach { star in
            star.tintColor = #colorLiteral(red: 0.8509803922, green: 0.8352941176, blue: 0.8235294118, alpha: 1)
        }
    }
}
