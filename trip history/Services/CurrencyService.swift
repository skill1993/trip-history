//
//  CurrencyService.swift
//  trip history
//
//  Created by Никита Журавлев on 04/01/2019.
//  Copyright © 2019 Никита Журавлев. All rights reserved.
//

import Foundation
import Alamofire

enum ServiceResult<Value> {
    case success(Value)
    case failure(Error)
}

class CurrencyService {
    
    private enum Constants {
        static let apiKey = "e24dde5f60adf7659e725d1a99db1cb3"
        static let baseURL = "http://data.fixer.io/api"
        static let error = NSError(domain: "CurrencyService", code: 0, userInfo: nil)
    }
    
    private enum MyConstants {
        static let appId = "20f388cc91a54b8d8fb60faf62cad303"
        static let apiKey = ""
        static let baseURL = "https://openexchangerates.org/api"
        static let error = NSError(domain: "CurrencyService", code: 300, userInfo: nil)
    }
    
    private enum Endpoints {
        static let latest = "/latest"
        static let anotherLatest = "/latest.json"
    }
    
    func obtainCurrency(for currencies: [Currency], completion: ((ServiceResult<CurrencyResult>) -> Void)?) {
        var urlComponents = URLComponents(string: Constants.baseURL + Endpoints.latest)
        let apiKey = URLQueryItem(name: "access_key", value: Constants.apiKey)
        let currencyString = currencies
            .map { $0.rawValue }
            .joined(separator: ",")
        let symbols = URLQueryItem(name: "symbols", value: currencyString)
        urlComponents!.queryItems = [apiKey, symbols]
        
        /*
        аналогия функции map
         
        var strings = [String]()
        for currency in currencies {
            strings.append(currency.rawValue)
        }
         */
        
        var request = URLRequest(url: urlComponents!.url!)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard error == nil else {
                completion?(.failure(error!))
                return
            }
            guard let data = data else {
                completion?(.failure(Constants.error))
                return
            }
            
            do {
                let payload = try JSONDecoder().decode(CurrencyResult.self, from: data)
                completion?(.success(payload))
            } catch let error {
                completion?(.failure(error))
            }
        }
        task.resume()
    }
    
    func obtainCurrency2(for currencies: [Currency], completion: ((Result<CurrencyResult>) -> Void)?) {
        
        let url = URL(string: Constants.baseURL + Endpoints.latest)!
        let currencyString = currencies
            .map { $0.rawValue }
            .joined(separator: ",")
        //let symbols = URLQueryItem(name: "symbols", value: currencyString)
        let parameters = ["access_key": Constants.apiKey,
                          "symbols": currencyString]
        
        let request = Alamofire.request(
            url,
            method: .get,
            parameters: parameters,
            encoding: URLEncoding.default,
            headers: nil)
        
        request.responseData { response in
            switch response.result {
            case .success(let data):
                do {
                    let payload = try JSONDecoder().decode(CurrencyResult.self, from: data)
                    completion?(.success(payload))
                } catch let error {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
    
    /* Пока не работает, надо копаться почему и как */
    func obtainUSDCurrency(for currencies: [Currency], completion: ((Result<MyCurrencyResult>) -> Void)?) {
        let url = URL(string: MyConstants.baseURL + Endpoints.anotherLatest)!
        let currencyString = currencies
            .map { $0.rawValue }
            .joined(separator: ",")
        let parameters = ["app_id": MyConstants.appId,
                          "symbols": currencyString]
        
        let request = Alamofire.request(
            url,
            method: .get,
            parameters: parameters,
            encoding: URLEncoding.default,
            headers: nil)
        
        request.responseData { response in
            switch response.result {
            case .success(let data):
                do {
                    let payload = try JSONDecoder().decode(MyCurrencyResult.self, from: data)
                    completion?(.success(payload))
                } catch let error {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
}
