//
//  PlaceCostViewController.swift
//  trip history
//
//  Created by Никита Журавлев on 14/11/2018.
//  Copyright © 2018 Никита Журавлев. All rights reserved.
//

import UIKit

class PlaceCostViewController: UIViewController,UITextFieldDelegate {
    
    var tempMoney: Int = 0
    var tempCurrency: String = "$"
    var tmpCur2: Currency = .dollar
    var tempSegmIndex: Int = 0
    var pay: CurrencyResult? = nil
    @IBOutlet weak var textFieldBackground: UIView!
    @IBOutlet weak var insertMoneyWastedLBL: UILabel!
    @IBOutlet weak var insertWastedMoneyTF: UITextField!
    @IBOutlet weak var currencySC: UISegmentedControl!
    
    @IBOutlet weak var doneButton: UIButton!
    
    let currencyList: [Currency] = [.dollar, .euro, .rouble]
    var currencyExpense: Money {
        let value = valueForText(insertWastedMoneyTF.text)
        let currency = currencyList[currencySC.selectedSegmentIndex]
        return Money(currency: currency, amount: Int(value))
    }
    
    private func valueForText(_ text: String?) -> Double {
        guard let text = text, let value = Double(text) else {
            return 0
        }
        return value
    }
    
    private func configureUI() {
        textFieldBackground.layer.cornerRadius = 6
        doneButton.layer.cornerRadius = 8
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
        
        self.currencySC.addTarget(self, action: #selector(selectedSegment), for: .valueChanged)
        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        
        tempMoney = Int(insertWastedMoneyTF.text ?? "0") ?? 0
        return true
    }
    
    @objc func selectedSegment(target: UISegmentedControl) {
        if target == self.currencySC {
            tempSegmIndex = currencySC.selectedSegmentIndex
            tempCurrency = currencySC.titleForSegment(at: tempSegmIndex) ?? tempCurrency
            switch tempCurrency{
            case "$": tmpCur2 = .dollar
            case "€": tmpCur2 = .euro
            case "₽": tmpCur2 = .rouble
            default: tmpCur2 = .dollar
            }
        }
    }
    
    @IBAction func backFromCurrencies(sender: UIStoryboardSegue) {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "show currencies" {
            let curVC = segue.destination as? CurrencyViewController
            print(pay!)
            curVC?.pay = pay
        }
        
    }
    
    /*
    func segmentValue(_ segmentedControl: UISegmentedControl) {
        tempSegmIndex = currencySC.selectedSegmentIndex
        tempCurrency = currencySC.titleForSegment(at: tempSegmIndex) ?? "$"
    }
    */
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
