//
//  DotsActivityIndicator.swift
//  trip history
//
//  Created by Никита Журавлев on 31/12/2018.
//  Copyright © 2018 Никита Журавлев. All rights reserved.
//

import UIKit

class DotsActivityIndicator: UIView {
    
    private enum AnimationKeys {
        static let group = "ScaleGruopAnimation"
    }
    
    private enum AnimationConstants {
        static let dotScale: CGFloat = 1.5
        static let scaleUpDuration: CFTimeInterval = 0.3
        static let scaleDownDuration: CFTimeInterval = 0.3
        static let lowOpacityDuration: CFTimeInterval = 0.1
        static let highOpacityDuration: CFTimeInterval = 0.1
        static let opacity: CGFloat = 0.35
        static let offset: CFTimeInterval = 0.12
        
        static var totalScaleDuration: CFTimeInterval {
            return scaleUpDuration + scaleDownDuration
        }
        
        static var totalOpacityDuration: CFTimeInterval {
            return lowOpacityDuration + highOpacityDuration
        }
    }
    
    var dots = [CALayer]()
    
    @IBInspectable
    var dotsCount: Int = 5 {
        didSet {
            removeDots()
            configureDots()
            setNeedsLayout()
        }
    }
    
    @IBInspectable
    var dotsRadius: CGFloat = 12 {
        didSet {
            dots.forEach { configureDotSize($0) }
            setNeedsLayout()
        }
    }
    
    @IBInspectable
    var dotsSpacing: CGFloat = 8 {
        didSet {
            setNeedsLayout()
        }
    }
    
    private var dotSize: CGSize {
        return CGSize.init(width: dotsRadius * 2, height: dotsRadius * 2)
    }
    
    private(set) var isAnimating: Bool = false
    
    override var tintColor: UIColor! {
        didSet {
            for layer in dots {
                layer.backgroundColor = tintColor.cgColor
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureDots()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureDots()
    }
    
    private func configureDots() {
        for _ in 0..<dotsCount {
            let dot = CALayer()
            dots.append(dot)
            configureDotSize(dot)
            configureDotColor(dot)
            layer.addSublayer(dot)
        }
        startAnimation()
    }
    
    private func configureDotSize(_ dot: CALayer) {
        dot.bounds = CGRect.init(origin: CGPoint.zero, size: dotSize)
        dot.cornerRadius = dotsRadius
    }
    
    private func configureDotColor(_ dot: CALayer) {
        dot.backgroundColor = tintColor.cgColor
    }
    
    private func removeDots() {
        dots.forEach { $0.removeFromSuperlayer() }
        dots.removeAll()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let center = CGPoint.init(x: frame.size.width / 2, y: frame.size.height / 2)
        let middle = dots.count / 2
        
        for i in 0..<dots.count {
            let x = center.x + CGFloat(i - middle) * (dotSize.width + dotsSpacing) + CGFloat(0.5 * (dotsSpacing + dotSize.width) * (CGFloat((dots.count - 1) % 2)))
            let y = center.y
            dots[i].position = CGPoint(x: x,y: y)
        }
    }
    
    private func opacityAnimation(_ after: TimeInterval) -> CAAnimationGroup {
        let opacityLow = CABasicAnimation(keyPath: "opacity")
        opacityLow.beginTime = after
        opacityLow.fromValue = 1
        opacityLow.toValue = AnimationConstants.opacity
        opacityLow.duration = AnimationConstants.lowOpacityDuration
        opacityLow.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        
        let opacityHigh = CABasicAnimation(keyPath: "opacity")
        opacityHigh.beginTime = after + opacityLow.duration
        opacityHigh.fromValue = AnimationConstants.opacity
        opacityHigh.toValue = 1
        opacityHigh.duration = AnimationConstants.highOpacityDuration
        opacityHigh.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        
        let group = CAAnimationGroup()
        group.animations = [opacityLow, opacityHigh]
        group.repeatCount = Float.infinity
        
        group.duration = CFTimeInterval(dots.count) * AnimationConstants.totalOpacityDuration
        
        return group
    }
    
    private func scaleAnimation(_ after: TimeInterval) -> CAAnimationGroup {
        let scaleUp = CABasicAnimation(keyPath: "transform.scale")
        scaleUp.beginTime = after
        scaleUp.fromValue = 1
        scaleUp.toValue = AnimationConstants.dotScale
        scaleUp.duration = AnimationConstants.scaleUpDuration
        // kCAMediaTimingFunctionLinear was renamed
        scaleUp.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        
        let scaleDown = CABasicAnimation(keyPath: "transform.scale")
        scaleDown.beginTime = after + scaleUp.duration
        scaleDown.fromValue = AnimationConstants.dotScale
        scaleDown.toValue = 1
        scaleDown.duration = AnimationConstants.scaleUpDuration
        // kCAMediaTimingFunctionLinear was renamed
        scaleDown.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
        
        let group = CAAnimationGroup()
        group.animations = [scaleUp, scaleDown]
        group.repeatCount = Float.infinity
        
        group.duration = CFTimeInterval(dots.count) * AnimationConstants.totalScaleDuration
        
        return group
    }
    
    func startAnimation() {
        isAnimating = true
        var offset: TimeInterval = 0
        dots.forEach {
            $0.removeAnimation(forKey: AnimationKeys.group)
            $0.add(opacityAnimation(offset) , forKey:  AnimationKeys.group)
            offset = offset + AnimationConstants.offset
        }
    }
    
    func stopAnimation() {
        isAnimating = false
        dots.forEach {
            $0.removeAnimation(forKey: AnimationKeys.group)
        }
    }
}

